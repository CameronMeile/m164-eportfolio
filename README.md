# M164 ePortfolio

### INDEX:
[TOC]

# Day 1: RECAP m162 [15.05.2023]

# Day 2: DDL [22.05.2023]

![GeneralisierungSpezialisierung](SpezialGeneral.png)

### Generalisierung
Es ist ein ideales Modell, um einheitliche strukturierte, aber inhaltlich differierende Tabellen mehrfach zu realisieren. Das kann z.B. ein umfangreiches Klassenmodell mit kaskadierenden Generalisierungen sein, realisiert wird die am Blatt stehende Entität.

### Spezialisierung
Die nicht gemeinsamen Attribute verleiben in den Entitätstypen.

### identifying relationship vs. non-identifying relationship
Zur Unterscheidung der identifying relationship von der non-identifying relationship gibt es extrem viel Schrott im Internet. Eine kurze einfache Erklärung gibt es hier : [MEHR ERFAHREN](http://www.datenbank-grundlagen.de/beziehungen-datenbanken.html)

Bei der identifying Beziehung ist der Fremdschlüssel Bestandteil des Indentifikationsschlüssels. DIe ID ist also eine aus mehereren Attributen zusammengesetzte Schlüsselkombination und der Datensatz die referenzierende Tabelle.

Eine non-identifying Beziehung wäre nicht so sinnvoll, denn darin könnte ich jederzeit den Fremdschlüsselwert ändern und den Raum einem anderen Gebäude zuordnen.

## 10 Best SQL datenbank
1. **Oracle** | Relational, Multi-model
2. **MySQL** | Relational, Multi-model
3. **Microsoft SQL Server** | Relational, Multi-model
4. **PostgreSQL** | Relational, Multi-model
5. **MongoDB** | Document, Multi-model
6. **Redis** | Key-value, Multi-model
7. **IBM Db2** | Relational, Multi-model 
8. **Elasticsearch** | Search engine, Multi-model
9. **SQLite** | Relational
10. **Microsoft Access** | Relational

## DBMS (Datenbank Management System)

![DatenbankSytem](DatenbankSytem.png)

Ein Datenbanksystem (DBS) ist ein System welches elektronische Daten verwaltet. Das wesentliche Ziel einer DBS ist es, Datenmengen effizient, widerspruchsfrei und dauerhaft zu speichern und benötigte Teilmengen in unterschiedlichen, bedarfsgerechen Darstellunsformen für Benutzer und Anwendungsprogramm bereitzustellen.

Eine DBS besteht aus Zwei Teilen:

- Eine Verwaltungssoftware, genannt Datenbankmanagementsystem (DBMS)
- Eine Datenbank welche die Daten verstaut werden.

Datenbanksystemen gibt es in verschiedenen Formen. Die Art und Weise, wie ein solches System Daten speichert und verwaltet, wird durch das Datnebankmodell festgelegt.

- Bekannteste Form: Relationale DatenbankSytem

### Merkmale eines DBMS
Gemäss der Definition muss ein DBMS folgende Funktionalitäten beiten:

- **Integrierte Datenhaltung** : Eine DBMS ermöglicht die einheitliche Verwaltung aller von den Anwendungen benötigten Daten.
- **Sprache** :
- **Katalog** :
- **Benutzersichten** :
- **Konsistenzkontrolle** :
- **Datenzugriffkontrolle** :
- **Transaktionen** :
- **Mehrbenutzerfähigkeit** :
- **Datensicherung** :

## DDL Einführung

# Konzept

![KOnzept](https://gitlab.com/ch-tbz-it/Stud/m164/-/raw/main/02_Theorie_Unterlagen/DDL/DDL.png)

## Forward Engineering
Forward Engineering bezieht sich auf den Prozess der Erstellung einer Datenbank aus einem Datenmodell. Das Datenmodell kann in einem Diagramm- oder Textformat vorliegen, das die Tabellen, Spalten, Beziehungen und Einschränkungen der Datenbank beschreibt.

Der Forward Engineering-Prozess beginnt mit der Erstellung eines **konzeptionellen Datenmodells**. In einem Datenbank-Design-Tool wie beispielsweise MySQL Workbench wird dieses Modell in ein **logisches Datenmodell** überführt. In diesem Tool können Sie das **Schema** der Datenbank verfeinern, indem Sie Entitäten und Attribute hinzufügen und Beziehungen zwischen diesen erstellen.  (Siehe Modul 162)

Durch die Definition der benutzten, DB-Engine-abhängigen Datentypen erhalten Sie nun das **physikalische Modell** zur Implementierung in das ausgewählte Datenbankmanagementsystems (DBMS). 

Dazu wird normalerweise ein ganzes **SQL-Skript** erstellt, das die CREATE-Anweisungen für die Tabellen und Einschränkungen (constraints) enthält ([DDL](https://www.w3schools.in/mysql/ddl-dml-dcl)). Dieses Skript wird dann im DBMS (MariaDB, MySQL, Oracle usw.) ausgeführt, um die Struktur der Datenbasis zu erstellen.

Der Vorteil von Forward Engineering besteht darin, dass es den Prozess der Datenbankerstellung automatisiert und Fehler reduziert. Indem das Datenmodell zuerst erstellt und dann in eine Datenbank umgewandelt wird, können Probleme und Inkonsistenzen frühzeitig erkannt werden, bevor die Datenbank im Einsatz ist.

Zusammenfassend ist Forward Engineering ein wichtiger Schritt im Prozess der Erstellung einer SQL-Datenbank aus einem Datenmodell. Es automatisiert den Prozess der Datenbankerstellung und minimiert menschliche Fehler, indem es das Datenmodell zuerst erstellt und dann in eine Datenbank umwandelt.

**Auftrag**: Setzen Sie den [Auftrag Forward Engineering](https://gitlab.com/ch-tbz-it/Stud/m164/-/blob/main/10_Auftraege_und_Uebungen/10_DDL/03_forward_engineering.md) um.

<br/>

# DDL-Statements

## CREATE DATABASE / SCHEMA

Siehe auch Kapitel 2 unter [Skript\_M164\_Themenübersicht](https://gitlab.com/ch-tbz-it/Stud/m164/-/blob/main/02_Theorie_Unterlagen/Skript_M164_Themenuebersicht.pdf)

CREATE SCHEMA ist eine SQL-Anweisung, die verwendet wird, um eine neue Datenbank in MySQL zu erstellen. Eine Datenbank in MySQL ist im Grunde genommen eine Sammlung von Tabellen, die zusammengehören und auf die gleiche Weise verwaltet werden. CREATE SCHEMA erstellt also eine leere Datenbank, in der Tabellen erstellt und Daten gespeichert werden können.

Die Syntax von CREATE SCHEMA ist wie folgt:

```sql
CREATE SCHEMA schema_name;
```

Hier ist "schema_name" der Name der Datenbank, die erstellt werden soll. Wenn Sie eine Datenbank erstellen, wird normalerweise der Standard-Charset und die Standard-Kollation verwendet. Diese können jedoch in der CREATE SCHEMA-Anweisung auch explizit angegeben werden:

```sql
CREATE SCHEMA schema_name
  DEFAULT CHARACTER SET utf8mb4
  DEFAULT COLLATE utf8mb4_unicode_ci;
```

In diesem Beispiel wird eine neue Datenbank namens "schema_name" erstellt, wobei UTF-8 als Zeichensatz und "utf8mb4_unicode_ci" als [Kollation](https://dev.mysql.com/doc/refman/8.0/en/charset-general.html) verwendet wird.

Sobald die Datenbank erstellt wurde, können Sie mit CREATE TABLE-Anweisungen Tabellen in der Datenbank erstellen und mit INSERT-Anweisungen Daten in die Tabellen einfügen.

Es ist zu beachten, dass CREATE SCHEMA in MySQL synonym zu CREATE DATABASE ist. CREATE SCHEMA und CREATE DATABASE können daher austauschbar verwendet werden. Beide Anweisungen führen letztendlich zur Erstellung einer neuen Datenbank in MySQL.

Das ganze soll nun **MANUELL** mit dem Klienten Workbench nachvollzogen werden:

![WB-SQL-Editor](https://gitlab.com/ch-tbz-it/Stud/m164/-/raw/main/02_Theorie_Unterlagen/DDL/SQL-Script_WB.png)

**Auftrag**: Setzen Sie den [Auftrag Create Schema](https://gitlab.com/ch-tbz-it/Stud/m164/-/blob/main/10_Auftraege_und_Uebungen/10_DDL/03_create_schema.md) um.

<br/>

## CREATE TABLE

Zu einer Tabelle ordnet man Attribute/ Columns zu. Dementsprechend sind Felddatendypen zu deklarieren.

CREATE TABLE ist eine SQL-Anweisung, die verwendet wird, um eine neue Tabelle in einer MySQL-Datenbank zu erstellen. Mit CREATE TABLE können Sie eine Tabelle erstellen und dabei die Spalten, Datentypen und Einschränkungen definieren, die in der Tabelle enthalten sein sollen.

Die Syntax von CREATE TABLE sieht wie folgt aus:

```sql
CREATE TABLE table_name (
    column1 datatype constraints,
    column2 datatype constraints,
    ...
    column_n datatype constraints
);
```

Hier ist "table_name" der Name der Tabelle, die erstellt werden soll. Jede Spalte wird durch den Namen und den Datentyp definiert. Der Datentyp gibt an, welchen Typ von Daten in der Spalte gespeichert werden soll, z. B. INTEGER, VARCHAR, DATE usw. Sie können auch Einschränkungen definieren, um sicherzustellen, dass die Daten in der Tabelle bestimmte Regeln oder Bedingungen erfüllen, z. B. die Anforderung, dass eine Spalte keine NULL-Werte enthalten darf oder dass ein eindeutiger Wert in einer Spalte vorhanden sein muss.

Ein Beispiel für eine CREATE TABLE-Anweisung könnte wie folgt aussehen:

```sql
CREATE TABLE customers (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    email VARCHAR(100) NOT NULL UNIQUE,
    age INT,
    PRIMARY KEY (id)
);
```
In diesem Beispiel wird eine Tabelle "customers" mit vier Spalten erstellt: "id", "name", "email" und "age". Die Spalte "id" ist ein INTEGER und wird als Primärschlüssel verwendet, um die Zeilen in der Tabelle zu identifizieren. Die Spalte "name" und "email" sind beide VARCHAR-Typen und haben die Einschränkung, dass sie nicht NULL sein dürfen. Die Spalte "email" hat auch die Einschränkung UNIQUE, was bedeutet, dass jeder Eintrag in der Spalte eindeutig sein muss. Die Spalte "age" ist optional und hat keinen Einschränkungen.

Insgesamt bietet CREATE TABLE eine flexible und leistungsstarke Möglichkeit, um Tabellen in MySQL-Datenbanken zu erstellen und anzupassen.

**Auftrag**: Setzen Sie den [Auftrag Create Table](https://gitlab.com/ch-tbz-it/Stud/m164/-/blob/main/10_Auftraege_und_Uebungen/10_DDL/03_create_table.md) um.

<br/>

## DROP TABLE
DROP TABLE ist eine SQL-Anweisung, die verwendet wird, um eine Tabelle in MySQL zu löschen. Wenn Sie eine Tabelle mit DROP TABLE löschen, werden alle Daten in der Tabelle permanent entfernt und können nicht wiederhergestellt werden. Es ist daher wichtig, sicherzustellen, dass Sie die richtige Tabelle löschen, bevor Sie DROP TABLE ausführen.

Die Syntax von DROP TABLE sieht wie folgt aus:

```sql
DROP TABLE table_name;
```

Hier ist "table_name" der Name der Tabelle, die gelöscht werden soll. Wenn die Tabelle erfolgreich gelöscht wird, gibt MySQL eine Bestätigungsmeldung zurück.

Es ist auch möglich, mehrere Tabellen in einer DROP TABLE-Anweisung zu löschen, indem Sie einfach die Namen der Tabellen durch Kommas trennen:

```sql
DROP TABLE table_name1, table_name2, table_name3;
```

Es gibt auch zusätzliche Optionen, die in DROP TABLE verwendet werden können, um das Verhalten der Anweisung anzupassen. Zum Beispiel können Sie die Option IF EXISTS verwenden, um sicherzustellen, dass MySQL keine Fehlermeldung zurückgibt, wenn die Tabelle, die Sie löschen möchten, nicht existiert:

```sql
DROP TABLE IF EXISTS table_name;
```

Insgesamt bietet DROP TABLE eine einfache Möglichkeit, um unerwünschte Tabellen in MySQL-Datenbanken zu entfernen und Speicherplatz freizugeben. Es ist jedoch wichtig, vorsichtig zu sein und sicherzustellen, dass Sie nur die Tabellen löschen, die Sie tatsächlich löschen möchten.

**Auftrag**: Setzen Sie den [Auftrag drop statement](https://gitlab.com/ch-tbz-it/Stud/m164/-/blob/main/10_Auftraege_und_Uebungen/10_DDL/03_drop_statement.md) um.

<br/>

## ALTER TABLE

Der Befehl ALTER TABLE in MySQL wird verwendet, um die **Struktur** einer vorhandenen Tabelle zu ändern. Mit diesem Befehl können Sie eine oder mehrere Spalten hinzufügen, ändern oder entfernen, den Datentyp einer Spalte ändern, einen neuen Index hinzufügen oder löschen und viele andere Änderungen an der Tabelle vornehmen.

Die Syntax für die Verwendung von ALTER TABLE lautet wie folgt:

```sql
ALTER TABLE table_name action;
```

"table_name" ist der Name der Tabelle, die geändert werden soll, und "action" ist die zu ändernde Aktion. Hier sind einige Beispiele für häufige Aktionen, die mit ALTER TABLE durchgeführt werden können:

**ADD** fügt eine Spalte hinzu:

```sql
ALTER TABLE table_name ADD column_name column_definition;
```

**RENAME COLUMN** verändert den Namen einer Spalte:

```sql
ALTER TABLE table_name RENAME COLUMN old_column_name TO new_column_name;
```

>
> Hinweis: Dieser Befehl geht erst ab MariaDB Version 10.5.2! Evtl. MySQL-Server [updaten](https://gitlab.com/ch-tbz-it/Stud/m164/-/tree/main/01_Installation_SW#mariadb-upgraden)!
> 


**CHANGE** ist der Befehl, um eine Spalte zu ändern, sowohl den Namen als auch ihre Definition. Beim Befehl gibt man den *alten* und *neuen* Namen sowie die *neue Definition* an. Wenn z.B. eine INT NOT NULL-Spalte von old_column_name in new_column_name umbenennen wollen und die Definition so geändert wird, dass sie den BIGINT-Datentyp verwendet, während das NOT NULL-Attribut beibehalten wird, gehen Sie wie folgt vor:

```sql
ALTER TABLE table_name CHANGE old_column_name new_column_name BIGINT NOT NULL;
```

**MODIFY** ist die elgegantere Variante, wenn der Name gleich bleibt aber die Definition ändert. Der Spaltenname muss deshalb nur einmal angegeben werden:

```sql
ALTER TABLE table_name MODIFY column_name new_data_type;
```

**DROP** ist der Befehl, zum Entfernen einer Spalte:

```sql
ALTER TABLE table_name DROP column_name;
```

Es gibt viele weitere Optionen, die mit ALTER TABLE durchgeführt werden können, um die Struktur einer Tabelle in MySQL zu ändern. Es ist jedoch wichtig zu beachten, dass einige Änderungen, wie das Hinzufügen oder Entfernen von Spalten, die bereits Daten in der Tabelle enthalten, zu Datenverlusten führen können, wenn die Änderungen nicht ordnungsgemäß durchgeführt werden.

**Auftrag**: Setzen Sie den [Auftrag alter table](https://gitlab.com/ch-tbz-it/Stud/m164/-/blob/main/10_Auftraege_und_Uebungen/10_DDL/05_alter_table.md) um.

# Quellen
https://dev.mysql.com/doc/refman/8.0/en/create-database.html<br>
https://dev.mysql.com/doc/refman/8.0/en/create-table.html<br>
https://dev.mysql.com/doc/refman/8.0/en/drop-table.html<br>
https://dev.mysql.com/doc/refman/8.0/en/alter-table.html<br>
https://dev.mysql.com/doc/refman/8.0/en/charset-general.html

```
